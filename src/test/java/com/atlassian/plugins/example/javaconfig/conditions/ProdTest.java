package com.atlassian.plugins.example.javaconfig.conditions;

import org.junit.Test;
import org.springframework.context.annotation.Condition;

import static com.atlassian.plugins.example.javaconfig.conditions.Environment.ENVIRONMENT_PROPERTY;
import static com.atlassian.plugins.example.javaconfig.conditions.Prod.ENVIRONMENT;
import static org.junit.Assert.assertEquals;

public class ProdTest {

    @Test
    public void matches_whenNoEnvironmentSet_shouldReturnFalse() {
        assertConditionMatches(null, false);
    }

    @Test
    public void matches_whenProdEnvironmentSet_shouldReturnTrue() {
        // Set up
        assertConditionMatches(ENVIRONMENT, true);
    }

    @Test
    public void matches_whenOtherEnvironmentSet_shouldReturnFalse() {
        // Set up
        assertConditionMatches("someOtherEnvironment", false);
    }

    @SuppressWarnings("ConstantConditions")
    private static void assertConditionMatches(final String environment, final boolean expectedResult) {
        // Set up
        if (environment == null) {
            System.clearProperty(ENVIRONMENT_PROPERTY);
        } else {
            System.setProperty(ENVIRONMENT_PROPERTY, environment);
        }
        final Condition condition = new Prod();

        // Invoke
        final boolean matches = condition.matches(null, null);

        // Check
        assertEquals(expectedResult, matches);
    }
}
