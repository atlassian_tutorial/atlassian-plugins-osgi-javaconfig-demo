package com.atlassian.plugins.example.javaconfig.impl;

import com.atlassian.plugins.example.javaconfig.api.MyDelegatingService;
import com.atlassian.sal.api.ApplicationProperties;

import java.security.SecureRandom;

import static java.util.Objects.requireNonNull;

/**
 * An internal component of this plugin. Note the lack of DI annotations in this entire class.
 */
public class MyDelegatingServiceImpl implements MyDelegatingService {

    public MyDelegatingServiceImpl(final ApplicationProperties applicationProperties) {
        requireNonNull(applicationProperties);
    }

    @Override
    public int delegatedNumber() {
        return new SecureRandom().nextInt();
    }
}
