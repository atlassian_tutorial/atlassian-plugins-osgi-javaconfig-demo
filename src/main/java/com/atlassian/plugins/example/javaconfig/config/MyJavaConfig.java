package com.atlassian.plugins.example.javaconfig.config;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugins.example.javaconfig.api.DevTimeService;
import com.atlassian.plugins.example.javaconfig.api.MyDelegatingService;
import com.atlassian.plugins.example.javaconfig.api.MyPluginComponent;
import com.atlassian.plugins.example.javaconfig.api.MySingletonService;
import com.atlassian.plugins.example.javaconfig.conditions.FalseCondition;
import com.atlassian.plugins.example.javaconfig.conditions.JavaVersionCondition;
import com.atlassian.plugins.example.javaconfig.conditions.Prod;
import com.atlassian.plugins.example.javaconfig.conditions.RequiresJava;
import com.atlassian.plugins.example.javaconfig.conditions.Stub;
import com.atlassian.plugins.example.javaconfig.impl.MyDelegatingServiceImpl;
import com.atlassian.plugins.example.javaconfig.impl.MyProdSingletonService;
import com.atlassian.plugins.example.javaconfig.impl.MyStubSingletonService;
import com.atlassian.plugins.example.javaconfig.moduletype.Greeter;
import com.atlassian.plugins.example.javaconfig.moduletype.GreeterModuleDescriptorFactory;
import com.atlassian.plugins.example.javaconfig.spi.ImplementMe;
import com.atlassian.plugins.osgi.javaconfig.annotations.ConditionalOnClass;
import com.atlassian.plugins.osgi.javaconfig.conditions.DevModeOnly;
import com.atlassian.plugins.osgi.javaconfig.conditions.product.JiraOnly;
import com.atlassian.plugins.osgi.javaconfig.configs.beans.ModuleFactoryBean;
import com.atlassian.plugins.osgi.javaconfig.configs.beans.PluginAccessorBean;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;

import java.util.List;
import java.util.Set;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.ImportOptions.defaultOptions;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportAsModuleType;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.factoryBeanForOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.factoryBeanForOsgiServiceCollection;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiServiceCollection;
import static com.atlassian.plugins.osgi.javaconfig.ServiceCollection.list;
import static com.atlassian.plugins.osgi.javaconfig.ServiceCollection.set;
import static java.time.Duration.ofSeconds;

/**
 * Demonstrates importing and exporting of Spring Beans to and from OSGi.
 */
@Configuration
@Import({
        ModuleFactoryBean.class,
        PluginAccessorBean.class
})
@ConditionalOnClass(ApplicationProperties.class)    // demo of this condition
public class MyJavaConfig {

    // Imports SAL's ApplicationProperties service from OSGi as a Spring bean
    @Bean
    public ApplicationProperties salApplicationProperties() {
        // Here we apply a filter to demonstrate filtering (in this case the filter is redundant, it's just an example)
        return importOsgiService(ApplicationProperties.class, defaultOptions().withFilter("(plugins-host=true)"));
    }

    // Imports Jira's ApplicationProperties service from OSGi as a Spring bean
    @Bean
    @Conditional(JiraOnly.class)
    public FactoryBean<com.atlassian.jira.config.properties.ApplicationProperties> jiraApplicationProperties() {
        return factoryBeanForOsgiService(com.atlassian.jira.config.properties.ApplicationProperties.class);
    }

    // Imports a list of SAL's PluginUpgradeTasks from OSGi
    @Bean
    public List<PluginUpgradeTask> salPluginUpgradeTasks() {
        return importOsgiServiceCollection(list(PluginUpgradeTask.class), defaultOptions().optional());
    }

    // Imports a set of Jira's PluginUpgradeTasks from OSGi
    @Bean
    @Conditional(JiraOnly.class)
    public FactoryBean<Set<PluginUpgradeTask>> jiraPluginUpgradeTasks() {
        return factoryBeanForOsgiServiceCollection(set(PluginUpgradeTask.class));
    }

    // A local component that depends on an OSGi service
    @Bean
    public MyDelegatingService delegatingService(final ApplicationProperties applicationProperties) {
        return new MyDelegatingServiceImpl(applicationProperties);
    }

    /*
        Imports Jira's IssueService from OSGi as a Spring bean, but only in Jira. Note that the method return type is a
        Spring FactoryBean, not an IssueService. This is to avoid Spring throwing a ClassNotFoundException about the
        IssueService interface when the product is not Jira. With the FactoryBean approach, the generic type of the
        FactoryBean will be erased by normal Java generics, so there is no reference to the IssueService class in the
        return type.

        Instead of returning a FactoryBean, another approach is to put the @Conditional annotation at the class level
        (instead of at the method level) and have the method return the IssueService interface; if the running product
        is not Jira, Spring will not introspect the config class' method signatures, and it will not matter that
        IssueService is not on the classpath. This of course assumes that all the beans in the annotated config class
        are only required to be loaded when the running product is Jira.
     */
    @Bean
    @Conditional(JiraOnly.class)
    public FactoryBean<IssueService> issueService() {
        return factoryBeanForOsgiService(IssueService.class);
    }

    // Exports the plugin's MyDelegatingService to OSGi
    @Bean
    @Conditional(DevModeOnly.class)
    public FactoryBean<ServiceRegistration> registerMyDelegatingService(
            final MyDelegatingService myDelegatingService) {
        return exportOsgiService(myDelegatingService, as(MyDelegatingService.class));
    }
    
    // This bean is always created
    @Bean
    public MyPluginComponent unconditionalBean() {
        return new MyPluginComponent() {
        };
    }

    // This bean is only created in the "prod" env
    @Bean
    @Conditional(Prod.class)
    public MySingletonService prodService() {
        return new MyProdSingletonService();
    }

    // This bean is only created in the "stub" env
    @Bean
    @Conditional(Stub.class)
    public MySingletonService stubService() {
        return new MyStubSingletonService();
    }

    // This bean is never created
    @Bean("shouldNotExist")
    @Conditional(FalseCondition.class)
    public MySingletonService shouldNeverBeCalled() {
        throw new IllegalStateException("Guarded by an always-false condition");
    }

    // This bean is only created in the "dev" profile
    @Bean
    @Conditional(DevModeOnly.class)
    public DevTimeService devTimeService() {
        return new DevTimeService() {
        };
    }

    // This bean is never created
    @Bean
    @Profile("noSuchProfile")
    public DevTimeService beanForInactiveProfile() {
        throw new IllegalStateException("This profile does not exist");
    }

    // A bean that requires Java 1.7
    @Bean
    @Conditional(JavaVersionCondition.class)
    @RequiresJava("1.7")
    public Object javaSevenBean() {
        return new Object();
    }

    // A bean that requires Java 1.8
    @Bean
    @Conditional(JavaVersionCondition.class)
    @RequiresJava("1.8")
    public Object javaEightBean() {
        return new Object();
    }

    // An optional bean with a short timeout to minimise unnecessary waiting
    @Bean
    public ImplementMe implementMe() {
        return importOsgiService(ImplementMe.class, defaultOptions().optional().withTimeout(ofSeconds(2)));
    }

    // ------------------------- Defines the greeter plugin point -------------------------

    @Bean
    public ListableModuleDescriptorFactory moduleDescriptorFactory() {
        return new GreeterModuleDescriptorFactory();
    }

    @Bean
    public FactoryBean<ServiceRegistration> greeterModuleType(
            final ListableModuleDescriptorFactory moduleDescriptorFactory) {
        return exportAsModuleType(moduleDescriptorFactory);
    }

    // ------------------------- Consumes the greeter plugin point -------------------------

    @Bean
    @Lazy // because eager doesn't find any instances (too fast?)
    public List<Greeter> greeters(final PluginAccessor pluginAccessor) {
        // Get the greeters defined in any plugin's XML
        return pluginAccessor.getEnabledModulesByClass(Greeter.class);
    }
}
