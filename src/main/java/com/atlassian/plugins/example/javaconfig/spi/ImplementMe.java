package com.atlassian.plugins.example.javaconfig.spi;

import javax.annotation.Nonnull;

/**
 * The SPI for this plugin. This plugin will perform an optional import of this service from OSGi.
 */
public interface ImplementMe {

    /**
     * Reports the name of the implementer.
     *
     * @return a name
     */
    @Nonnull
    String whoAreYou();
}
