package com.atlassian.plugins.example.javaconfig.web;

import com.atlassian.plugins.example.javaconfig.moduletype.Greeter;

import javax.annotation.Resource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.List;

import static java.util.stream.Collectors.joining;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 * A REST resource to demonstrate the {@link Greeter} plugin point.
 */
@Path("greet")
public class GreetingResource {

    @Resource
    private List<Greeter> greeters;

    @GET
    @Path("person")
    @Produces(TEXT_PLAIN)
    public String greet(@QueryParam("name") final String name) {
        return greeters.stream()
                .map(greeter -> greeter.greet(name))
                .collect(joining(","));
    }
}
