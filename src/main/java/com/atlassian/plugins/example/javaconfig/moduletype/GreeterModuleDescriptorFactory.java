package com.atlassian.plugins.example.javaconfig.moduletype;

import com.atlassian.plugins.osgi.javaconfig.moduletypes.PluginContextModuleDescriptorFactory;

/**
 * A sample {@link com.atlassian.plugin.ModuleDescriptorFactory}.
 */
public class GreeterModuleDescriptorFactory extends PluginContextModuleDescriptorFactory<GreeterModuleDescriptor> {

    public GreeterModuleDescriptorFactory() {
        super("greeter", GreeterModuleDescriptor.class);
    }
}
