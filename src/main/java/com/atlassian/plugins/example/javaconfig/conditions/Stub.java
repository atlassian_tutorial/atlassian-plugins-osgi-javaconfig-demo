package com.atlassian.plugins.example.javaconfig.conditions;

public class Stub extends Environment {

    public static final String ENVIRONMENT = "stub";

    public Stub() {
        super(ENVIRONMENT);
    }
}
