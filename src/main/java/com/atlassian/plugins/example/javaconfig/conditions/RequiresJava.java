package com.atlassian.plugins.example.javaconfig.conditions;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that the annotated method creates a Spring bean that requires a given version of Java.
 * Intended for use in conjunction with the {@link JavaVersionCondition}.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiresJava {

    /**
     * The required version of Java.
     *
     * Matched against the return value of <code>System#getProperty("java.version")</code>.
     *
     * @return e.g. "1.8", "1.8.0", or "1.8.0_92"
     */
    String value();
}
