package com.atlassian.plugins.example.javaconfig.web;

import com.atlassian.plugins.example.javaconfig.spi.ImplementMe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 * A REST resource that invokes a service that is optionally imported from OSGi.
 */
@Path("spi")
public class SpiInvokingResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpiInvokingResource.class);

    private final ImplementMe implementMe;

    public SpiInvokingResource(final ImplementMe implementMe) {
        this.implementMe = requireNonNull(implementMe);
    }

    @GET
    @Path("who")
    @Produces(TEXT_PLAIN)
    public String whoIsImplementingMySpi() {
        try {
            return implementMe.whoAreYou();
        } catch (final RuntimeException e) {
            return handleSpiInvocationException(e);
        }
    }

    private static String handleSpiInvocationException(final RuntimeException e) {
        // We check by the class name only because this Gemini Blueprint exception type is not on the classpath
        if ("ServiceUnavailableException".equals(e.getClass().getSimpleName())) {
            // In a real plugin, you would implement your fallback logic here
            return "Could not import an implementation of the ImplementMe SPI from OSGi.";
        }
        LOGGER.error("Error invoking ImplementMe service", e);
        return "Unexpected error - see product log for details.";
    }
}
