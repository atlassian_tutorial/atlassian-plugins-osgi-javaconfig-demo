package com.atlassian.plugins.example.javaconfig.api;

/**
 * Empty interface for us to implement.
 */
public interface MyPluginComponent {}
