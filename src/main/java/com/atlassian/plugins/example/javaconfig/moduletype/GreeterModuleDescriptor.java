package com.atlassian.plugins.example.javaconfig.moduletype;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import org.dom4j.Element;

import javax.annotation.Nonnull;

import static com.atlassian.extras.common.org.springframework.util.StringUtils.countOccurrencesOf;
import static java.lang.String.format;

/**
 * A sample {@link com.atlassian.plugin.ModuleDescriptor}.
 */
public class GreeterModuleDescriptor extends AbstractModuleDescriptor<Greeter> {

    private String greetingFormat;

    public GreeterModuleDescriptor(final ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public Greeter getModule() {
        return new StringFormatGreeter(greetingFormat);
    }

    @Override
    public Class<Greeter> getModuleClass() {
        return Greeter.class;
    }

    @Override
    public void init(@Nonnull final Plugin plugin, @Nonnull final Element element) throws PluginParseException {
        super.init(plugin, element);
        this.greetingFormat = element.attribute("format").getValue();
        final int placeholderCount = countOccurrencesOf(greetingFormat, "%s");
        if (placeholderCount > 1) {
            throw new PluginParseException(
                    format("Greeting format should contain at most one '%%s' placeholder; found %d", placeholderCount));
        }
    }
}
