package com.atlassian.plugins.example.javaconfig.api;

public interface MyDelegatingService {
    int delegatedNumber();
}
