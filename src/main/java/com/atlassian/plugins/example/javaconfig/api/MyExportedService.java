package com.atlassian.plugins.example.javaconfig.api;

/**
 * A dummy service that we export to OSGi.
 */
public interface MyExportedService {}
